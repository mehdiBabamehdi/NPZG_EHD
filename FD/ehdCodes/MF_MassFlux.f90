

Subroutine MF_MassFlux
   
Use NEnumber

Implicit None

! ##############################################################################
! IN THE SUBROUTINE, MASS FLUX PASSED THROUGH FACES OF VOLUMES BY USING 
! RIE AND CHOE PROPOSED (momentum interpolation method) METHOD ARE CALCULATED

! Ufmi & Vfmi : VALUE OF VELOCITY ON FACE
! Uhmi & Vhmi : VALUE OF VELOCITY ON FACE WITHOUT ADDING PRESSURE TERMS
! MASSF : MASS FLUX
! MASSF1 : INLET MASS FLUX 
! MASSF2 : OUTLET MASS FLUX 
! ##############################################################################


Integer         :: m1,m2

Real (Kind = 8) :: dx,dy, &
                   massf1,massf2, &
                   ae

   
   massf1 = 0.0
   massf2 = 0.0
   do i = 1,E
      do j = 1,4
         select case (j)
            case(1)
              m1 = 1
              m2 = 2
            case(2)
              m1 = 2
              m2 = 3
            case(3)
              m1 = 3
              m2 = 4
            case(4)
              m1 = 4
              m2 = 1
         end select
         dx = x(Con(i,m2)) - x(Con(i,m1))
         dy = y(Con(i,m2)) - y(Con(i,m1))
         
         if(Neibr(i,j) /= 0) then
            ae = (f(i,j) * ap(Neibr(i,j)) + (1.0 - f(i,j)) * ap(i))    
            Ufmi(i,j) = Uhmi(i,j) - alphaU * dy * (P(Neibr(i,j)) - P(i)) &
                        / ae + alphaU * ((1.0 - f(i,j)) * ST_x(i) + f(i,j) * ST_x(Neibr(i,j))) / ae
            Vfmi(i,j) = Vhmi(i,j) + alphaV * dx * (P(Neibr(i,j)) - P(i)) &
                        / ae + alphaV * ((1.0 - f(i,j)) * ST_y(i) + f(i,j) * ST_y(Neibr(i,j))) / ae
         else
           Ufmi(i,j) = Uf(i,j)
           Vfmi(i,j) = Vf(i,j)
         end if

         massf(i,j) = ru * (Ufmi(i,j) * dy - Vfmi(i,j) * dx)
         if(Fp(i,j) == 4)   massf1 = massf1 + massf(i,j)
         if(FP(i,j) == 2)   massf2 = massf2 + massf(i,j)
      end do
   end do

   m_ratio = abs(massf1 / massf2)

   if(m_ratio < 1.0) then   
      m_ratio = exp(m_ratio - 1.0)
   end if

Return

End Subroutine MF_MassFlux

