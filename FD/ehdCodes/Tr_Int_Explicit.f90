

Subroutine Tr_Int_Explicit

Use NEnumber		

Implicit None
   
Integer       :: Nt,m1,m2

Real (Kind=8) :: difInt, &
				 Intmex(E,4),Rint,Siint


  do i=1,E
	do t=1,4
		if(Neibr(i,t)/=0) then
			if (BNC(i,t)/=0) then
				if (AdI==1.0) 	then
					Intmex(i,t)=(-1.0*intm(BNC(i,t))-2.0*intm(i)+3.0*intm(FNC(i,t)))/8.0	! QUICK
				else if (AdI==2.0) 	then
					Rint=(intm(i)-intm(BNC(i,t)))/(intm(FNC(i,t))-intm(i))
					Siint=max(Z,min(2.0*Rint,(3.0+Rint)/4.0,2.0))							! QUICK Limiter
					Intmex(i,t)=Siint*(intm(FNC(i,t))-intm(i))/2.0
				else if (AdI==3.0) 	then
					Rint=(intm(i)-intm(BNC(i,t)))/(intm(FNC(i,t))-intm(i))
					if(Rint>0.0)  Siint=min(Rint,1.0)										! Mid-Mod Limiter
					if(Rint<=0.0) Siint=0.0
					Intmex(i,t)=Siint*(intm(FNC(i,t))-intm(i))/2.0
				else if (AdI==4.0) 	then
					Rint=(intm(i)-intm(BNC(i,t)))/(intm(FNC(i,t))-intm(i))
					Siint=max(z,min(2.0*Rint,1.0),min(Rint,2.0))							! SUPERBEE Limiter
					Intmex(i,t)=Siint*(intm(FNC(i,t))-intm(i))/2.0
				else if (AdI==5.0) 	then
					Rint=(intm(i)-intm(BNC(i,t)))/(intm(FNC(i,t))-intm(i))
					if (Rint<0.0 .or. intm(FNC(i,t))==intm(i)) then
						Siint=0.0
					else
						Siint=(Rint+abs(Rint))/(1.0+Rint)									! Van Leer Limiter
					end if
					Intmex(i,t)=Siint*(intm(FNC(i,t))-intm(i))/2.0
				end if				
			else
				Intmex(i,t)=(intm(FNC(i,t))-intm(i))/2.0
			end if			
		end if
	end do
  end do

  CALL TranNodal

   do i=1,E
      Int_Exp(i)=0.0 !ru*ds(i)/dt*Intm(i)
	  do j=1,4
         select case (j)
	       case(1)
	        m1=1
		    m2=2
           case(2)
   	        m1=2
		    m2=3
           case(3)
			m1=3
		    m2=4
		   case(4)
			m1=4
		    m2=1
         end select
		if (Neibr(i,j)/=0) then
			do t=1,4
				if(i==Neibr(Neibr(i,j),t)) then
					Nt=t
					exit
				end if
			end do

			 difInt=min(Z,massf(i,j))*Intmex(Neibr(i,j),Nt)+max(Z,massf(i,j))*Intmex(i,j)
			 Int_Exp(i)=Int_Exp(i)+(mu+Mut(i)/sgmf)*K(i,j)/M(i,j)*(Intmn(Con(i,m2))-Intmn(Con(i,m1)))-difInt 
		end if
	  end do
	end do

Return

End Subroutine Tr_Int_Explicit
