

Subroutine MF_Corrections
   
Use NEnumber

Implicit None

! ##############################################################################
! IN THE SUBROUTINE, FINAL STEP OF SIMPLER ALGORITHM (CORRECTION) IS APPLIED 
! FOR VELOCITIES. 
! IN ADDITION, ERRORS FOR VELOCITIES BASED ON FOLLOWING FORMULA IS CALCULATED:
! ERROR=SIGMA (Un-Un-1)/SIGMA(AP*Un)	
! Un: VALUE OF CURRENT STEP	
! Un-1 : VALUE OF PREVIOUS STEP
! SIGMA : SUM OF ALL CELLS' VALUE
! ##############################################################################

Integer         :: m1,m2

Real (Kind = 8) :: dx,dy,Vold(E),Uold(E),FrU,FrV

   
   ErrorU = 0.0
   FrU = 0.0
   ErrorV = 0.0
   FrV = 0.0
   do i = 1,E
!     P(i) = P(i) + alphaP * Pcor(i)
      do j = 1,4
         select case (j)
             case(1)
               m1 = 1
               m2 = 2
             case(2)
               m1 = 2
               m2 = 3
             case(3)
               m1 = 3
               m2 = 4
             case(4)
               m1 = 4
               m2 = 1
        end select
        
        dx = x(Con(i,m2)) - x(Con(i,m1))
        dy = y(Con(i,m2)) - y(Con(i,m1))
        
        if(Neibr(i,j) /= 0) then
           U(i) = U(i) - alpha1 * dy / ap(i) * ((1.0 - f(i,j)) * Pcor(i) + f(i,j) * Pcor(Neibr(i,j)))
           V(i) = V(i) + alpha2 * dx / ap(i) * ((1.0 - f(i,j)) * Pcor(i) + f(i,j) * Pcor(Neibr(i,j)))
!          U(i) = U(i) - alphaV * dy / 2.0 / ap(i) * (Pcor(i) + Pcor(Neibr(i,j)))
!          V(i) = V(i) + alphaV * dx / 2.0 / ap(i) * (Pcor(i) + Pcor(Neibr(i,j)))
           Ufmi(i,j) = Ufmi(i,j) + alpha1 * dy * (Pcor(i) - Pcor(Neibr(i,j))) / ((1.0-f(i,j)) * ap(i) + f(i,j) * ap(neibr(i,j)))
           Vfmi(i,j) = Vfmi(i,j) + alpha2 * dx * (Pcor(i) - Pcor(Neibr(i,j))) / ((1.0-f(i,j)) * ap(i) + f(i,j) * ap(neibr(i,j)))
!       else if (FP(i,j) == 2) then
!          U(i) = U(i) - alphaV * dy / ap(i) * Pcor(i) / 2.0
!          V(i) = V(i) + alphaV * dx / ap(i) * Pcor(i) / 2.0			  
        else
           U(i) = U(i) - alpha1 * dy / ap(i) * Pcor(i)
           V(i) = V(i) + alpha2 * dx / ap(i) * Pcor(i)
        end if
      end do
      ErrorU = ErrorU + abs(Uold(i) - U(i))
      FrU = FrU + abs(U(i))
      Uold(i) = U(i)

      ErrorV = ErrorV + abs(Vold(i) - V(i))
      FrV = FrV + abs(V(i))
      Vold(i) = V(i)  
   end do
   ErrorU = ErrorU / FrU
   ErrorV = ErrorV / FrV
   
Return  

End Subroutine MF_Corrections
