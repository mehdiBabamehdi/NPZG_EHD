

Subroutine Tr_Int_Prod

Use NEnumber

Implicit None


Real (Kind=8) :: Fonset,Fonset1,Fonset2,Fonset3,Fturb


	do i=1,E
		Rev(i)=ru*wd(i)*wd(i)*Str(i)/mu
		Fonset1=Rev(i)/2.193/Remc(i)
		Fonset2=min(max(Fonset1,Fonset1**4.0),2.0)
		Rt(i)=ru*Kt(i)/mu/Omega(i)
		Fonset3=max((1.0-(Rt(i)/2.5)**3.0),0.0)
		Fonset=max(Fonset2-Fonset3,0.0)		
		Prod_int1(i)=Flength(i)*Ca1*ru*Str(i)*(abs(intm(i)*Fonset))**0.5*ds(i)
		
		Fturb=exp(-(Rt(i)/4.0)**4.0)
		Prod_int2(i)=Ca2*ru*Vor(i)*intm(i)*Fturb*ds(i)

		Prod_int(i)=Prod_int1(i)+Prod_int2(i)
	end do

Return

End Subroutine Tr_Int_Prod



