 
 Subroutine Output


 use GCNEnumber

 implicit none

 integer:: i


26  Format(4I6)
	
	 do i = 1,E 
		write (100,26) FP(i,1:4)
	 end do

	 close(100)

	 open(10,file='C:\MY FOLDER\Project\Code\CFD\Flat Plate\EHD2CFD\Grid\Grid Character\ZFP_GG_ECFDt2.plt')

	 Rewind (10)

	 write(10,*)'VARIABLES = "X", "Y"'
	 write(10,*)'ZONE N=',N,', E=',E,', F=FEPOINT, ET=QUADRILATERAL, C=RED'  

31  Format(A31,1X,6I6)
	 write (10,31) '# Number of Boundary Divisions:',n2,m10,m11,m12,m13,m14

311 Format(A34,1X,6E18.8)
	 write (10,311) '# Dimention of Boundary Portions:',H,L0,L1,L2,L3,L4

21  Format(2E24.5)

	 do i = 1,N
		Write (10,21) X(i),Y(i)
	 end do

22  Format(4I7)
	 do i = 1,E
		write(10,22) Con(i,1:4)
	 end do

	 Close (10)

 Return

 End Subroutine Output
