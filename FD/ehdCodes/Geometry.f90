

Subroutine Geometry 

Use NEnumber

Implicit None

! ##############################################################################
! IN THE SUBROUTINE, DATA FROM GEOMETRO OF PROBLEM IS OBTAINED (THIS DATA WAS 
! ACHEIVED BEFORE IN DISTINCT PROGRAMS). AND THEN GEOMETRY CHARACTER AND TERMS 
! IN EQUATION BY USING THAT DATA ARE CALCULATED.
! ##############################################################################
   
Integer        :: m1,m2
character :: form
Real (kind = 8):: Xn,Yn, &
                  ndw(E),dx,dy,surf1,surf2, &
                  Fbt_x(E),Fbt_y(E)

Common /PBFt/ Fbt_x,Fbt_y
! .... Reading Grid Data .............................................................

   Open(10,file = ".\DATA\T3C4\NZFP_GG_ECFD-T3C4.plt")
   Open(20,file = ".\DATA\T3C4\NZFP_GC_ECFD_T3C4.dat")
   Open(30,file = ".\DATA\T3C4\Body_Force-12v-3h-nzpg-T3C4-PSTW.plt")
      
   
12 format(////,2E24.5)
   read(10,12) x(1),y(1)

13 format(2E24.5)
   do i=2,N
      read(10,13) x(i),y(i)
   end do

14 format(4I7)
   do i=1,E
      read(10,14) Con(i,1:4)
      Xp(i) = (x(Con(i,4)) + x(Con(i,3)) + x(Con(i,2)) + x(Con(i,1))) / 4.0
      Yp(i) = (y(Con(i,4)) + y(Con(i,3)) + y(Con(i,2)) + y(Con(i,1))) / 4.0
   end do

   close(10)

! .... Reading Grid Characters .........................................................
 
   f2=0.0
   do i=1,N
      read(20,'(I7)') CV(i,1)
      write( form, '(a,i0,a)' )  '(', CV(i,1), 'i7)' 
      backspace(20)
!15 format(T8,CV(i,1)(I7))
      read(20,form) CV(i,2:CV(i,1)+1)
      do j=1,CV(i,1)
          f2(i,j) = 1.0 / sqrt((x(i) - Xp(CV(i,j+1))) * (x(i) - Xp(CV(i,j+1))) &
                    + (y(i) - Yp(CV(i,j+1))) * (y(i) - Yp(CV(i,j+1))))
         f2(i,20) = f2(i,20) + f2(i,j)
      end do
   end do
      

   do i=1,N
      f2(i,1:CV(i,1)) = f2(i,1:CV(i,1)) / f2(i,20)
   end do   
   
16 format(4I7)
   do i = 1,E
      read(20,16) Neibr(i,1:4)
   end do

   do i = 1,E
      ds(i) = 0.0
      do j = 1,4
	       select case (j)
	          case(1)
	            m1 = 1
	            m2 = 2
           case(2)
   	          m1 = 2
	            m2 = 3
           case(3)
    	        m1 = 3
	            m2 = 4
	         case(4)
    	        m1 = 4
	            m2 = 1
         end select
	 
         dx = x(Con(i,m2)) - x(Con(i,m1))
         dy = y(Con(i,m2)) - y(Con(i,m1))
         
         if(Neibr(i,j) == 0) then
	          Xn = (x(Con(i,m2)) + x(Con(i,m1))) / 2.0
	          Yn = (y(Con(i,m2)) + y(Con(i,m1))) / 2.0
         else
	          Xn = Xp(Neibr(i,j))
	          Yn = Yp(Neibr(i,j))
         end if 
	 
         surf1 = sqrt((Xp(i) - (x(Con(i,m1)) + x(Con(i,m2))) / 2.0) * (Xp(i) - (x(Con(i,m1)) + x(Con(i,m2))) / 2.0) &
	               + (Yp(i) - (y(Con(i,m1)) + y(Con(i,m2))) / 2.0) * (Yp(i) - (y(Con(i,m1)) + y(Con(i,m2))) / 2.0))
	     surf2 = sqrt((Xn - (x(Con(i,m1)) + x(Con(i,m2))) / 2.0) * (Xn - (x(Con(i,m1)) + x(Con(i,m2))) / 2.0) &
	               + (Yn - (y(Con(i,m1)) + y(Con(i,m2))) / 2.0) * (Yn - (y(Con(i,m1)) + y(Con(i,m2))) / 2.0))
         f(i,j) = surf1 / (surf1 + surf2)
         K(i,j) = -(Xn - Xp(i)) * dx - (Yn - Yp(i)) * dy
         L(i,j) = dy * dy + dx * dx
         M(i,j) = (Xn - Xp(i)) * dy - (Yn - Yp(i)) * dx
         ds(i)  = ds(i) + (x(Con(i,m1)) * y(Con(i,m2)) - y(Con(i,m1)) * x(Con(i,m2))) / 2.0
     end do
   end do
     
! .... Reading Data For Applying QUICK Or TVD Method ....................................... 

23  FORMAT(2I7)

    do i = 1,E
	     do j = 1,4
	        read(20,23) BNC(i,j),FNC(i,j)
	     end do
    end do
		
! .... Reading Normal Distance Oo Each Cell Center From Nearest Wall .........................

35  FORMAT(E24.5)

    do i = 1,E
	     read(20,35) wd(i)
	     if (Xp(i) < 0.0) wd(i) = 0.3
    end do

	! wd  : nearest distance from wall (Turbulence model requirement)

! .... Reading Position Of Each Cell In Domain ...............................................

21  FORMAT (4I7)

    do i = 1,E 
	     read(20,21) FP(i,1:4)
    end do

    close(20)

! ... Reading Data of EHD Body Force Applied As a Source Term ..................................

22 Format (///,4E25.6)
    Read (30,22) Xp(1),Yp(1),Fbt_x(1),Fbt_y(1)

24 Format (4E25.6)

    Do i = 2,E
	     Read (30,24) Xp(i),Yp(i),Fbt_x(i),Fbt_y(i)
    end do      
	
    Close (30)

Return

End Subroutine Geometry
