

Subroutine Tu_KE_Explicit

Use NEnumber		

! ##############################################################################
! IN THE SUBROUTINE, EXPLICIT TERM OF KINETIC ENERGY (K) EQUATION (OF DIFFISION 
! AND CONECTION TERMS)IS CALCULATED. IN CALCULATION OF CONVECTION'S, FIVE DIFFERENT 
! METHODS ARE USED. DETAILS OF METHOD CAN BE FOUND IN  VERSTEEG AND MALALASEKRA'S BOOK [1]
!	
! KEex : EXPLICIT TERM OF CONVECTION TERM
! KE_EX : SUM OF EXPLICIT TERMS OF CONVECTION AND DIFFUSION 
!
! --- REFERENCES: ---------------
! [1] Versteeg & Malalasekra, "An Introduction to Computational Fluid Dynamics" 2nd Ed., PEARSON PUB., 2007.
! ############################################################################## 
Implicit None
   
Integer       :: Nt,m1,m2

Real (Kind=8) :: KEex(E,4),RKt,SiKt,ConKt


  do i=1,E
	do t=1,4
		if(Neibr(i,t)/=0) then
			if (BNC(i,t)/=0) then
				if (AdK==1.0) 	then
					KEex(i,t)=(-1.0*Kt(BNC(i,t))-2.0*Kt(i)+3.0*Kt(FNC(i,t)))/8.0	! QUICK
				else if (AdK==2.0) 	then
					RKt=(Kt(i)-Kt(BNC(i,t)))/(Kt(FNC(i,t))-Kt(i))
					SiKt=max(Z,min(2.0*RKt,(3.0+RKt)/4.0,2.0))						! QUICK Limiter
					KEex(i,t)=SiKt*(Kt(FNC(i,t))-Kt(i))/2.0
				else if (AdK==3.0) 	then
					RKt=(Kt(i)-Kt(BNC(i,t)))/(Kt(FNC(i,t))-Kt(i))
					if(RKt>0.0) SiKt=min(RKt,1.0)									! Mid-MOd Limiter
					if(RKt<=0.0) SiKt=0.0
					KEex(i,t)=SiKt*(Kt(FNC(i,t))-Kt(i))/2.0
				else if (AdK==4.0) 	then
					RKt=(Kt(i)-Kt(BNC(i,t)))/(Kt(FNC(i,t))-Kt(i))
					SiKt=max(z,min(2.0*RKt,1.0),min(RKt,2.0))						! SUPERBEE Limiter
					KEex(i,t)=SiKt*(Kt(FNC(i,t))-Kt(i))/2.0
				else if (AdK==5.0) 	then
					RKt=(Kt(i)-Kt(BNC(i,t)))/(Kt(FNC(i,t))-Kt(i))
					if (RKt<0.0 .or. Kt(FNC(i,t))==Kt(i)) then
						SiKt=0.0
					else
						SiKt=(RKt+abs(RKt))/(1.0+RKt)									! Van Leer Limiter
					end if
					KEex(i,t)=SiKt*(Kt(FNC(i,t))-Kt(i))/2.0
				end if		
			else
				KEex(i,t)=(Kt(FNC(i,t))-Kt(i))/2.0
			end if			
		end if
	end do
  end do

  CALL TurbNodal

   do i=1,E
      KE_Exp(i)=0.0	!betas*ru*Omegai*Kti !+ru*ds(i)*Kt(i)/dt
	  do j=1,4
         select case (j)
	       case(1)
	        m1=1
		    m2=2
           case(2)
   	        m1=2
		    m2=3
           case(3)
			m1=3
		    m2=4
		   case(4)
			m1=4
		    m2=1
         end select
		if (Neibr(i,j)/=0) then
			do t=1,4
				if(i==Neibr(Neibr(i,j),t)) then
					Nt=t
					exit
				end if
			end do
			ConKt=min(Z,massf(i,j))*KEex(Neibr(i,j),Nt)+max(Z,massf(i,j))*KEex(i,j)
			KE_Exp(i)=KE_Exp(i)+(mu+sgmk(i)*Mut(i))*K(i,j)/M(i,j)*(Ktn(Con(i,m2))-Ktn(Con(i,m1)))-ConKt
		end if
	  end do
	end do

Return

End Subroutine Tu_KE_Explicit