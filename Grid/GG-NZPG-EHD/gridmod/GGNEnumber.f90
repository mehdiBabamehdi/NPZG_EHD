
!__________________________________________________________________________________

    Module GGNEnumber

    integer,parameter:: M=100, & !     M: Number of divisions of Length, in the X direction  
	                    N=50    !     N: Number of divisions of Width, in the Y direction  
    
    real::  Lf=2.0, &		!    Lf: Length of flat plate 
	        W=0.3 , &		!     W: Value of Width (dimension normal the flow direction)
	        Alphax=1.0, &	! Alpha: Parameter indicates the position of gridlines clustering: 0 for x=0 
			Betax=8.0, &	!  Beta: Parameter indicates the intension of gridlines clustering
			Alphay=0.0, &	! Alpha: Parameter indicates the position of gridlines clustering: 0 for y=0 
			ds=3.0e-5		!    ds: Height Of First Row in Y Direction 
 
    end module GGNEnumber

