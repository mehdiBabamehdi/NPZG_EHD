


Subroutine MF_Coefficients

Use NEnumber

Implicit None

! ##############################################################################
! IN THE SUBROUTINE, COEFFICIENTS OF DISCRITISED NS EQUATION BASED ON FV METHOD 
! ARE CALCULATED. DUE TO USING GUASS'S THEOREM IN CALCULATING INTEGRAL, 
! TURBULENT VISCOSITY ON FACE NEEDS TO BE CALCULATED.
! DUE TO SATISFICTION OF MASS FLUX IN CELL, MASS FLUX IS REMOVED FROM 
! CALCULATION OF AP.
! ##############################################################################


Real (kind = 8) :: Mue


   do i = 1,E
      ap(i) = 0.0 !ru*ds(i)/dt
      do j = 1,4
         if (Neibr(i,j) == 0) then
            if (FP(i,j) == 1) then     ! Solid Boundary (Mut=0)
               Mue = 0.0
            else
               Mue = Mut(i)
            end if
         else
            Mue = (Mut(i) + Mut(Neibr(i,j))) / 2.0
         end if
         an(i,j) = max(Z,-massf(i,j)) + (mu+Mue) * L(i,j) / M(i,j)
         ap(i) = ap(i) + an(i,j)    !+massf(i,j)
      end do
   end do
   
Return

End Subroutine MF_Coefficients

