
!__________________________________________________________________________________

Subroutine Initial

Use NEnumber

Implicit None

! ##############################################################################
! IN THE SUBROUTINE, INITIAL GUESS AND VALUES ARE SET.
! ##############################################################################

Character(30):: fname,SS


   print*,"DO YOU WANT TO RESUME OR CONTINUE? Type R: RESUME & C: CONTINUE "
   Read*, SS

   if (SS == 'C') then 
      !print*,'Please Enter Name Of File That Contains Solved Data'
      !read(*,'(A30)') fname
      open(45,file = ".\DATA\cellvalue.dat")

      do i=1,E
         read(45,'(9E24.5)') U(i),V(i),P(i),Kt(i),Omega(i),Mut(i),Intm(i),Rehmt(i),Remt(i)
      end do
      close(45)
   else

! .... Mean Flow Initial Value ......................................  

      U    = Ui
      V    = 0.0
!     Un   = 0.01
!     Vn   = 0.01
      P    = 0.0001
      Pcor = 0.00
      P0   = 0.0

! .... Turbulence Initial Value ..................................... 

      Mut   = Rmu * mu
      kt    = Kti
      Omega = Omegai
      
! .... Transition Initial Value .....................................
  
      intm = 1.0
      Remt = Remti
      Rehmt = Remti
   end if
   Eff_int = 1.0

Return

End Subroutine Initial
