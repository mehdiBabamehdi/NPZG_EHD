

Module NEnumber
   
   Integer,Parameter :: N = 128961, E = 128000, & 
		        n1 = 800 ,  n2 = 160 ,  n3 = 800 ,  n4 = 160 ,  n5 = 0 ,  n6 = 0 ,  mo = n4+n1+n2+n3 , &	! Number of Cells
			mb = n6+n1+n2+n3+n4	, &
			bt = 5, 	&	  ! Number Of X-Cross Section Whose Boundary Layer Thickness Are Calculated In Post Processing
			sp = 670	  	  ! Position of Results

   Real,Parameter    :: alphaU = 0.1 ,  alphaV = 0.1, &      !  Velocity Under Relaxation Factor For SIMPLER Method
			alphaP = 0.1 ,  &  !1.0-alphaV, &    !  Pressure Under Relaxation Factor For SIMPLER Method
			alpha1 = 0.1 ,  alpha2 = 0.1, &	     !  Underrelaxation For Time Step
                        Alphak = 0.2 ,  AlphaO = 0.3, &	     !  Turbulence Parameter Under Relaxation Factor 
                        AlphaI = 0.3 ,  AlphaR = 0.3, &	     !  Transition Parameter Under Relaxation Factor 

! ... Turbulence Model Coefficient .............................................

			betas = 0.09   , a1    = 0.31    , kapa  = 0.41			 , &
			alp1  =  0.47  , beta1 = 0.0750  , sgmk1 = 0.85 , sgmo1 = 0.65   , &
			alp2  =  0.44  , beta2 = 0.0828  , sgmk2 = 1.0  , sgmo2 = 0.856  , &
					
! ... Transition Model Coefficient .............................................

			Ce1   = 1.0 , Ca1 = 2.0 , Ce2 = 50.0 , Ca2 = 0.06 , Sgmf = 1.0   , &
			S1    = 2.0                                                 , &
			Sgmmt = 2.0 , Cmt = 0.03 , &
			Z     = 0.0
	
   Integer           :: Con(E,4) , CV(N,20) , Neibr(E,4), &	    ! GRID CHARACTERISTICS	
			BNC(E,4) , FNC(E,4), &
			FP(E,4)  , PC(E), &

			AdU , AdV , AdK , AdO , AdI , AdR, &	    ! SCHEME OF CONVECTION TERM
			CFS , &				 	    ! EMPERICAL FORMULA FOR TRANSITION MODEL

			cell_maxYP , POS, &		  	    ! POST-PROCESSING DATA
			im , &				 	    ! ERROR DATA

			i , j , t				    ! COUNTERS
	
	
   Real (kind=8)     :: x(N) , y(N) , xp(E) , yp(E) , K(E,4) , L(E,4) , M(E,4) , f(E,4) , f2(N,20) , ds(E) , wd(E), &	! GRID CHARACTERISTICS

! ... NAVIER - STOCKS EQUATIONS .............................................

                        uh(E) , vh(E) , u(E) , v(E) , Ut(E) , P(E) , Pcor(E) , Px(E) , Py(E) , massf(E,4) , m_ratio, &		! N-S CHARACTERS
			Un(N) , Vn(N) , Pn(N), &					! NODAL VALUES
			Uex(E,4) , Vex(E,4) , uf(E,4) , vf(E,4), &			! FACIAL VALUES
			Ufmi(E,4) , Vfmi(E,4) , Uhmi(E,4) , Vhmi(E,4), &		! MATRIX COEFFICIENT
			ap(E) , an(E,4), &						! EXPLICIT TERMS
			ExpliciteX(E) , ExpliciteY(E), &
			Ux(E) , Uy(E) , Vx(E) , Vy(E), &				! VELOCITY GRADIENT

! ... Turbulence Model .............................................

			kt(E) , Omega(E) , Ktn(N) , Omegan(N) , Ktf(E,4) , Omgf(E,4), &		! TURBULENT CHARACTERS
			Kx(E) , Ky(E) , Omgx(E) , Omgy(E), &					! GRADIENT OF TURBULENT CHARACTERS
			Mut(E), &								! TURBULENT VISCOSITY
			apk(E) , ank(E,4) , apom(E) , anom(E,4), &				! MATRIX COEFFICIENT
			KE_Exp(E) , Omg_Exp(E), &						! EXPLICIT TERMS			   
			Phk(E) , Prod_Omg(E),CD(E), &						! PRODUCTION & DISSIPATION TERMS
			Dis_k(E) , Dis_omg(E), &
			Spk(E) , Sck(E) , Spomg(E) , Scomg(E), &
			Ft1(E) , Ft2(E), &							!  
			alp(E) , beta(E),sgmk(E),sgmo(E), &					! 
			Str(E) , Vor(E),Strt12(E), &						! VORTICITY & STRAIN RATE

! ... Transition Model .............................................

			intm(E) , Rehmt(E) , Remt(E) , Intmn(N) , Rehmtn(N) , Remtn(N) , Intmf(E,4) , Rehmtf(E,4), &	! TRANSITION CHARACTERS
			apmt(E) , anmt(E,4) , apin(E) , anin(E,4), &
			Eff_int(E) , Eff_intn(N), &							! EFFECTIVE INTERMITTENCY
			Rt(E) , Rev(E), &
			Fmt(E), &				  
			Flength(E) , Remc(E), &
			Prod_int(E) , Des_int(E) , Prod_Rehmt(E), &					! PRODUCTION & DISSIPATION TERMS
			Prod_int1(E) , Prod_int2(E), &
			Sp_int(E) , Sc_int(E) , Sp_Rehmt(E) , Sc_Rehmt(E), &
			Int_Exp(E) , Rehmt_Exp(E), &							! EXPLICIT TERMS
					  				      
			kti , Omegai , Ktinf , Omginf , intmi , Remti, &
			Tul, &
			er , ermax , ErrorU , ErrorV , Errork , ErrorO , ErrorI , ErrorR, &
			Y_Pmax , Y_Pmin, &

! ... EHD Body Force .............................................
					  
			Fb_x(E) , Fb_y(E) , ST_x(E) , ST_y(E)
 
   Real              :: mu , ru , dt , T_Time , Ui , P0 , Re, &			! PROPERTIES
			op_time , Lc, &
			Tui , Rmu

					

End Module NEnumber
