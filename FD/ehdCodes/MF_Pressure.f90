﻿

Subroutine MF_Pressure(i2)
   
Use NEnumber

! ##############################################################################
! IN THE SUBROUTINE, PRESSURE EQUATION OF NS EQUATION (POAISON EQUATION) ARE CALCULATED (STEP 3,6)
! A DOMINATED BODY FORCE AS A SOURCE TERM IS ADDED TO NS EQUATIONS. THE WAY IT SHOULD BE TREATED
! CAN BE FOUND IN REF [1]
!	
! P : P*
! Pcor : CORRECTION OF P (P')
! B : EXPLICIT TERMS OF PRESSURE EQUATIONS
! anp & app : COEFFICIENT OF PARAMETERS (P* & P')
!
!--- REFERENCES: ---------------
! [1] J. Mencinger and I. Zun. On the ﬁnite volume discretization of discontinuous body force ﬁeld on
!     collocated grid: Application to vof method. Journal of Computational Physics, 221:524–538, 2007.
! ############################################################################## 

Implicit None

Integer       :: i2,m1,m2

Real (Kind=8) :: app,anp,apnp,B,sumP,dx,dy,ae,BFT,Ff_X,Ff_Y

   
   do i = 1 , E
      app  = 0.0
      B    = 0.0
      bft  = 0.0
      sumP = 0.0
!	  if (PC(i)==2) then
!		  P(i)=P0
!		  Pcor(i)=0.0
!	  else
     do j = 1 , 4
        anp = 0.0
        select case (j)
           case(1)
              m1 = 1
              m2 = 2 
           case(2)
              m1 = 2
              m2 = 3
           case(3)
              m1 = 3
              m2 = 4
           case(4)
              m1 = 4
              m2 = 1
        end select
        dx = x(Con(i,m2)) - x(Con(i,m1))
        dy = y(Con(i,m2)) - y(Con(i,m1))
        
        if (FP(i,j) == 1 .and. i2 == 1) then       
            Ff_X = Fb_x(i) + (Fb_x(i) - Fb_x(i+n1)) / (yp(i+n1) - yp(i)) * yp(i)
            Ff_y = Fb_y(i) + (Fb_y(i) - Fb_y(i+n1)) / (yp(i+n1) - yp(i)) * yp(i)
                      
            BFT = (-(Ff_X) * dy * ds(i) + (Ff_y) * dx * ds(i)) / (0.5 * ap(i))      ! PLASMA BODY FORCE ON THE FLAT PLATE SURFACE
            B = B + BFT
        end if              
        
        if(Neibr(i,j) /= 0) then
!          ae=1.0/(f(i,j)/ap(Neibr(i,j))+(1.0-f(i,j))/ap(i))
!          anp=L(i,j)/ae
           anp = L(i,j) / (f(i,j) * ap(Neibr(i,j)) + (1.0-f(i,j)) * ap(i))
!          anp=L(i,j)/(ap(Neibr(i,j))+ap(i))*2.0
           app = app + anp
           
           if(i2 == 1) then
              sumP = sumP + anp * P(Neibr(i,j))
              B    = B - (Uhmi(i,j) * dy - Vhmi(i,j) * dx)
              BFT  = (-((1.0 - f(i,j)) * Fb_x(i) + f(i,j) * Fb_x(Neibr(i,j))) * dy * ds(i) + ((1.0 - f(i,j)) * Fb_y(i) &
                     + f(i,j) * Fb_y(Neibr(i,j))) * dx * ds(i)) / (f(i,j) * ap(Neibr(i,j)) + (1.0 - f(i,j)) * ap(i))    ! A DOMINATED BODY FORCE AS A SOURCE TERM
              B = B + BFT
!             B = B-(((1.0-f(i,j))*Uh(i)+f(i,j)*Uh(Neibr(i,j)))*dy-((1.0-f(i,j))*Vh(i)+f(i,j)*Vh(Neibr(i,j)))*dx)
           else
              sumP = sumP + anp * Pcor(Neibr(i,j))
              B = B - massf(i,j) / ru!((U(i)+U(Neibr(i,j)))*dy-(V(i)+V(Neibr(i,j)))*dx)/2.0	
           end if
        else
                  
!			 if (FP(i,j)==2) then
!				apnp=L(i,j)/ap(i)
!				app=app+apnp
!				if (i2==1) then
!					sumP=sumP+apnp*P0
!					B=B-(Uh(i)*dy-Vh(i)*dx)
!				else
!					B=B-massf(i,j)/ru	!(U(i)*dy-V(i)*dx)	
!				end if
!			 else
           B = B - massf(i,j) / ru
!			 end if
        end if
     end do
             
     if(i2 == 1) then
        P(i) = (alphaP * (sumP+B)) / app + (1.0 - alphaP) * P(i)
     else
        Pcor(i) = (sumP + B) / app
     end if
!	   end if
   end do
   
Return

End Subroutine MF_Pressure
