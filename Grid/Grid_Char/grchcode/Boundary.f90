
subroutine Boundary

use GCNEnumber

implicit none

double precision :: x(N),y(N)

integer :: Con(E,4),Neibr(E,4),CV(N,20),NCV(N), &
		       i,t,k,NBT, &
		       FP(E,4)
 
common /geom/ x,y,Con
common /neibr/ Neibr
common /faceP/FP
common /BNUM/ NBT

  
!  ====================================================================================
!  BN  : TRIANGULAR THAT HAS THE POINT LOCATES ON BOUNDARY AND SHARE IT WITH 
!
!  BNF : FACE OF ABOVE TRIANGULARE THAT LOCATES ON BOUNDARY
!  ====================================================================================

  NBT = 0
  do i = 1,E
	do t = 1,4
		if (Neibr(i,t)==0) then
			NBT = NBT + 1

19		   FORMAT(I7,2I3)

			if(FP(i,t)==1) then			! Lower Face
				k = 1
				write(100,19) i,t,k

			else if(FP(i,t)==2) then	! Left Face
				k = 2
				write(100,19) i,t,k		

			else if(FP(i,t)==3) then	! Upper Face
				k = 3
				write(100,19) i,t,k

			else if(FP(i,t)==4) then	! Right Face
				k = 4		
				write(100,19) i,t,k

			else if(FP(i,t)==6) then	! Airfoil Face
				k = 5
				write(100,19) i,t,k

			else if(FP(i,t)==7) then	! Symetric Face
				k = 7
				write(100,19) i,t,k

			end if
		end if
	end do
  end do



return
 	
end subroutine Boundary
