
!__________________________________________________________________________________

    Subroutine PotenIter (NI,kt)

!    Use Geom
    Use ProbCons
     
    Implicit none

    Integer :: i,l,k,m1,m2,kt,NI
    Real :: LP,MP
	Real (Kind=8) :: ap(E),an(E,4),RHS(E), &
	                 Pn,P0,p_1(E)

	Call Explicit_Term(kt)
	
	ap=0.0
	do i=1,E
		RHS(i)=Explicit(i)
		if (NCI/=0.0) then 
			if (Xp(i)>X(m10+m11+m12) .and. Xp(i)<x(NCI+m10+m11+m12)) then 
				ap(i)=(1.0/(Eps*Landa*Landa))*A(i)
			else 
				ap(i)=0.0
			end if
		end if
		do l=1,4
			LP=GP(i,l,2)
			MP=GP(i,l,3)
			an(i,l)=-LP/MP
			ap(i)=ap(i)-an(i,l)

			if(Se(i,l)==1 .or. Se(i,l)==2) RHS(i)=RHS(i)-an(i,l)*0.0
			if(Se(i,l)==3) RHS(i)=RHS(i)-an(i,l)*P(i)
			if(Se(i,l)==4) RHS(i)=RHS(i)-an(i,l)*Vout(kt)
			if(Se(i,l)==5) then
				select case(l)
					case(1)
						m1=Con(i,1)
						m2=Con(i,2)
					case(2)
						m1=Con(i,2)
						m2=Con(i,3)
					case(3)
						m1=Con(i,3)
						m2=Con(i,4)
					case(4)
						m1=Con(i,4)
						m2=Con(i,1)
				end select    
				RHS(i)=RHS(i)-an(i,l)*(Pv(m1)+Pv(m2))/2.0
			end if
        end do
		
	end do


	p_1(1:E)=p(1:E)
	if (mod(NI,IOR)==0) then 
		Call GMRES_m (RHS,an,ap,Neib,E,4,p)
	else 		
		Call GS (RHS,an,ap,Neib,E,4,p)		
	end if
	
	if (mod(NI,IOR)==0) then
		er=0.0
		do i=1,E
			if(abs(P(i)-P_1(i))/maxval(abs(P))>er)    er=abs(P(i)-P_1(i))/maxval(abs(P))
		end do
	end if

	    
 Return

 End Subroutine PotenIter
