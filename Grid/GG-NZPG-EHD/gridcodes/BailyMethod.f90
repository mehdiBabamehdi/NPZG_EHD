
Subroutine BailyMethod

use GGNEnumber

implicit none

integer:: i,j,k 
double precision:: BG, &
				   f,fp,fz,x1,x2,error
real::betay

Common /Del/ BG,betay

x1 = 1.0
	do 
		f = sinh(x1) / x1 - BG
		fp = (x1 * cosh(x1) - sinh(x1)) / x1**2.0
		fz = ((cosh(x1) + x1 * sinh(x1) - cosh(x1)) * x1 - 2.0 * (x1 * cosh(x1) - sinh(x1))) / x1**3.0
		x2 = x1 - (f / (fp - f * fz / 2.0 / fp))
		error = abs(x2 - x1)
		if (error<1.0e-10) then
			betay = x2
			exit
		end if
		x1 = x2
	end do
	print*, 'Expansion Ratio In Y direction:' , betay

Return

End Subroutine BailyMethod

