
!__________________________________________________________________________________

subroutine MF_Velocity(i2)

Use NEnumber

! ##############################################################################
! IN THE SUBROUTINE, VELOCITY IN X AND Y DIRECTION INCLUDING STEP 4 & 7 ARE CALCULATED.
! Uhmi IS CALCULATED BY USING FORMULA No. 10 IN MAJUMDAR PAPER [1].

!	
! Uh & Vh : VELOCITY WHICH CALCULATED IN STEP 2
! Uhb & Vhb : VALUE OF VELOCITY IN BOUNDARY
! Uhmi & Vhmi : THE VELOCITY WHICH IS INTRODUCED BY MAJUMDAR [#] BY REMOVING PRESSURE TERMS

! --- REFFERENCES: ----------------
! [1] S. Majumdara, "ROLE OF UNDERRELAXATION IN MOMENTUM INTERPOLATION FOR CALCULATION OF FLOW WITH NONSTAGGERED GRIDS", Numerical Heat Transfer
!     Volume 13, Issue 1, 1988.

! ############################################################################## 

Implicit None

Integer       :: i2,m1,m2

Real (Kind=8) :: cx1,cy1,u1,v1,Vnb,Unb


   do i = 1 , E
      Uh(i) = ExpliciteX(i) / ap(i)
      Vh(i) = ExpliciteY(i) / ap(i)
      
      do j = 1 , 4   
         if(Neibr(i,j) /= 0) then   
            Unb = u(Neibr(i,j))
            Vnb = v(Neibr(i,j))
         else
            Unb = Uf(i,j)
            Vnb = Vf(i,j)
         end if
         Uh(i) = Uh(i) + an(i,j) * Unb / ap(i)
         Vh(i) = Vh(i) + an(i,j) * Vnb / ap(i)
      end do
      
      if(i2 /= 1) then
         u(i) = alphaU * Uh(i) + (1.0 - alphaU) * U(i) - alphaU * Px(i) / ap(i) + alphaU * ST_x(i) / ap(i)
         v(i) = alphaV * Vh(i) + (1.0 - alphaV) * V(i) - alphaV * Py(i) / ap(i) + alphaV * ST_y(i) / ap(i)
      end if
   end do
   
   do i = 1 , E
      do j = 1 , 4
         if(Neibr(i,j) /= 0) then  
            Uhmi(i,j) = alphaU * ((1.0 - f(i,j)) * Uh(i) + f(i,j) * Uh(neibr(i,j))) + &
                        (1.0 - alphaU) * (Ufmi(i,j))  !-(1.0-f(i,j))*U(i)-f(i,j)*U(neibr(i,j)))
            Vhmi(i,j) = alphaV * ((1.0 - f(i,j)) * Vh(i) + f(i,j) * Vh(neibr(i,j))) + &
                        (1.0 - alphaV) * (Vfmi(i,j))  !-(1.0-f(i,j))*V(i)-f(i,j)*V(neibr(i,j)))
         end if
      end do
   end do
 
Return

End Subroutine MF_Velocity

