

Subroutine TurbNodal
   
Use NEnumber

Implicit None

Integer       :: m1,m2

Real (Kind=8) :: WDM
   

    Ktn=0.0
    Omegan=0.0
	do i=1,N
		do j=1,CV(i,1)
			Ktn(i)=Ktn(i)+Kt(CV(i,j+1))*f2(i,j)
			Omegan(i)=Omegan(i)+Omega(CV(i,j+1))*f2(i,j)
		end do
	end do

	do i=1,E
		do t=1,4
			if (Neibr(i,t)==0) then
				select case (t)
	 			  case(1)
	 				m1=1
	 				m2=2
		  		  case(2)
	    			m1=2
	 				m2=3
		  		  case(3)
	 		 		m1=3
	 				m2=4
				  case(4)
	 		 		m1=4
	 				m2=1
				end select
				
				if (FP(i,t)==7) then		! Symmetric Surface Upstream of the Flat Plate
					if (CV(Con(i,m1),1)/=1 .and. CV(Con(i,m2),1)/=1) then				
						Ktn(Con(i,m1))=(Ktf(CV(Con(i,m1),2),t)+Ktf(CV(Con(i,m1),3),t))/2.0
						Ktn(Con(i,m2))=(Ktf(CV(Con(i,m2),2),t)+Ktf(CV(Con(i,m2),3),t))/2.0
						Omegan(Con(i,m1))=(Omgf(CV(Con(i,m1),2),t)+Omgf(CV(Con(i,m1),3),t))/2.0
						Omegan(Con(i,m2))=(Omgf(CV(Con(i,m2),2),t)+Omgf(CV(Con(i,m2),3),t))/2.0
					end if

	            else if (FP(i,t)==1) then
					Ktn(Con(i,m1))=0.0	
					Ktn(Con(i,m2))=0.0

					if (CV(Con(i,m1),1)/=1) then
						wdm=(wd(CV(Con(i,m1),2))+wd(CV(Con(i,m1),3)))/2.0	
						Omegan(Con(i,m1))=60.0*mu/(ru*beta1*wdm**2.0)
					else 
						Omegan(Con(i,m1))=60.0*mu/(ru*beta1*wd(CV(Con(i,m1),2))**2.0)
					end if

					if (CV(Con(i,m2),1)/=1) then		
						wdm=(wd(CV(Con(i,m2),2))+wd(CV(Con(i,m2),3)))/2.0	
						Omegan(Con(i,m2))=60.0*mu/(ru*beta1*wdm**2.0)
					else 
						Omegan(Con(i,m2))=60.0*mu/(ru*beta1*wd(CV(Con(i,m2),2))**2.0)
					end if	

				else if (FP(i,t)==2) then
					if (CV(Con(i,m1),1)/=1 .and. CV(Con(i,m2),1)/=1) then				
						Ktn(Con(i,m1))=(Ktf(CV(Con(i,m1),2),t)+Ktf(CV(Con(i,m1),3),t))/2.0
						Ktn(Con(i,m2))=(Ktf(CV(Con(i,m2),2),t)+Ktf(CV(Con(i,m2),3),t))/2.0
						Omegan(Con(i,m1))=(Omgf(CV(Con(i,m1),2),t)+Omgf(CV(Con(i,m1),3),t))/2.0
						Omegan(Con(i,m2))=(Omgf(CV(Con(i,m2),2),t)+Omgf(CV(Con(i,m2),3),t))/2.0
					end if

				else if (FP(i,t)==3) then
					if (CV(Con(i,m1),1)/=1 .and. CV(Con(i,m2),1)/=1) then				
						Ktn(Con(i,m1))=(Ktf(CV(Con(i,m1),2),t)+Ktf(CV(Con(i,m1),3),t))/2.0
						Ktn(Con(i,m2))=(Ktf(CV(Con(i,m2),2),t)+Ktf(CV(Con(i,m2),3),t))/2.0
						Omegan(Con(i,m1))=(Omgf(CV(Con(i,m1),2),t)+Omgf(CV(Con(i,m1),3),t))/2.0
						Omegan(Con(i,m2))=(Omgf(CV(Con(i,m2),2),t)+Omgf(CV(Con(i,m2),3),t))/2.0
					end if
				
				else if (FP(i,t)==4) then
						Ktn(Con(i,m1))=Kti
						Ktn(Con(i,m2))=Kti
						Omegan(Con(i,m1))=Omegai
						Omegan(Con(i,m2))=Omegai

				else if (FP(i,t)==6) then		! Surface of Airfoil
					Ktn(Con(i,m1))=0.0	
					Ktn(Con(i,m2))=0.0

					if (CV(Con(i,m1),1)/=1) then
						wdm=(wd(CV(Con(i,m1),2))+wd(CV(Con(i,m1),3)))/2.0	
						Omegan(Con(i,m1))=60.0*mu/(ru*beta1*wdm**2.0)
					else 
						Omegan(Con(i,m1))=60.0*mu/(ru*beta1*wd(CV(Con(i,m1),2))**2.0)
					end if

					if (CV(Con(i,m2),1)/=1) then		
						wdm=(wd(CV(Con(i,m2),2))+wd(CV(Con(i,m2),3)))/2.0	
						Omegan(Con(i,m2))=60.0*mu/(ru*beta1*wdm**2.0)
					else 
						Omegan(Con(i,m2))=60.0*mu/(ru*beta1*wd(CV(Con(i,m2),2))**2.0)
					end if			
				end if
			end if
		end do
	end do

Return

End Subroutine TurbNodal