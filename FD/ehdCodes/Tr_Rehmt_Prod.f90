

Subroutine Tr_Rehmt_Prod

Use NEnumber

Implicit None


Real (Kind=8) :: Tmt,Mtbl,Dtbl,Delta,Reomega,Fwake,Fmt1,Fmt2,Fmt3


	do i=1,E
		Tmt=500.0*mu/(ru*Ut(i)*Ut(i))
		Mtbl=Rehmt(i)*mu/ru/Ut(i)
		Dtbl=15.0/2.0*Mtbl
		Delta=50.0*Vor(i)*wd(i)*Dtbl/Ut(i)
		Reomega=ru*Omega(i)*wd(i)*wd(i)/mu
		Fwake=exp(-(Reomega*10.0**(-5.0))**2.0)
		Fmt1=Fwake*exp(-(wd(i)/Delta)**4.0)
		fmt2=1.0-((intm(i)-1.0/Ce2)/(1.0-1.0/Ce2))**2.0
		Fmt3=max(Fmt1,Fmt2)
		Fmt(i)=min(Fmt3,1.0)

		Sp_Rehmt(i)=Cmt*ru/Tmt*(1.0-Fmt(i))*ds(i)
		Sc_Rehmt(i)=Cmt*ru/Tmt*(1.0-Fmt(i))*Remt(i)*ds(i)
		!Prod_Rehmt(i)=Cmt*ru/Tmt*(Remt(i)-Rehmt(i))*(1.0-Fmt(i))*ds(i)
	end do

Return

End Subroutine Tr_Rehmt_Prod


