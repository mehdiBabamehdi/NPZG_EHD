

!include 'NEnumber.f90'

program FPGrid

!__ Main Program: _________________________________________________________________

!use portlib
use GGNEnumber

implicit none

integer:: i,j,k,nt,t
double precision:: x(M + 1,N + 1),y(M + 1,N + 1),XI,ETA,BG,S,L,Lu
real:: a,b,c,d,e,f,betay

Common /geom/ x,y
Common /Del/ BG,betay

i = 1
j = 1
k = 0
Lu = 0.2
L = Lf + Lu		!     L: Length of Domain in Streamwise Direction


do i = 1,21
		x(i,1) = -Lu + Lu / 20.0 * (i - 1)
	end do

do i = 23,m + 2
	    b = 1.0 + (exp(betax) - 1.0) * (alphax / Lf)
		c = 1.0 + (exp( - betax) - 1.0) * (alphax / Lf)
		A = log(b / c) / 2.0 / betax
		ETA = real(i - 22) / real(M - 21)
		d = sinh(betax * (ETA - A))
		e = sinh(betax * A)
		f = 1.0 + d / e
		X(i - 1,1) = alphax * f
	end do
		 
   do i = 1,m + 1
	  if (X(i,1)<1.7) then
   		  x(i,n + 1) = X(i,1)
                  
		  !y(i,n + 1) = W * min(1.356 * (x(i,n + 1)**6.0) - 7.591 * (x(i,n + 1)**5.0) + 16.513 * (x(i,n + 1)**4.0) &
				   ! - 17.510 * (x(i,n + 1)**3.0) + 9.486 * (x(i,n + 1)**2.0) - 2.657 * x(i,n + 1) + 0.991,1.0)
                   y(i,n + 1)  =  5.0 * 0.09 * (0.2969 * sqrt(x(i,n + 1)) - 0.1260 * x(i,n + 1) - 3.5169 * x(i,n + 1)**2.0 &
                              + 0.2843 * x(i,n+1)**3.0 - 0.1015 * x(i,n + 1)**4.0)
		nt = i
	  else
		  x(i,n + 1) = X(i,1)
		  y(i,n + 1) = y(nt,n + 1)
	  end if	
   end do

   BG = 1.0 / real(n) / dS

   Call BailyMethod
   
   do j = 2,n
      do i = 1,m + 1
	     S = sinh(betay * (j - 1) / n) / sinh(betay)
		 x(i,j) = x(i,1) + (x(i,n + 1) - x(i,1)) * S
         y(i,j) = y(i,1) + (y(i,n + 1) - y(i,1)) * S
      end do
   end do
   
   	    
 Print*, ""
 Print*, " ... EXAMINATION OF GENERATED GRID'S CHARACTERISTICS ............."

 Print*, "Number of Nodes:", M * N
 Print*, "Number of Cells:", (M - 1) * (N - 1)
 do i = 1,M
	if (x(i,1)<0.0 .and. x(i + 1,1)>0.0) then
		print*,"Aspect Ratio in Leading Edge:", abs((x(i,1) - x(i + 1,1)) / (y(i,1) - y(i,2)))
	end if
 end do

 t = 1
 print*,"Aspect Ratio in Trailing Edge:", abs((x(M,1) - x(M + 1,1)) / (y(M,1) - y(M,2)))
 Print*,"Expansion Ratio in Y Direction:", abs((y(1,t + 2) - y(1,t + 1)) / (y(1,t + 1) - y(1,t)))

 Print*, " ................................................................."
 Call Output

 print*,">> See the Geometry Tecplot File!"

 pause
contains

!.. Subroutines: ..................................................................

include 'Output.f90'
include 'BailyMethod.f90'

!__________________________________________________________________________________

end program FPGrid
