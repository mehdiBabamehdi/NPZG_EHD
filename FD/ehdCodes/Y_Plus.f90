

Subroutine Y_Plus

Use NEnumber

Implicit None


Real (Kind=8) :: Y_Pmax1,Y_Pmin1,WSS,nus,Y_P(E)


	Y_Pmax=0.0
	Y_Pmin=1000.0
	do i=1,E
		Y_Pmax1=Y_Pmax
		Y_Pmin1=Y_Pmin
		if (FP(i,1)==1 .or. FP(i,2)==1 .or. FP(i,3)==1 .or. FP(i,4)==1) then
			WSS=mu*U(i)/wd(i)
			nus=sqrt(abs(WSS/ru))
			Y_P(i)=wd(i)*nus*ru/mu
			if (Y_P(i)>Y_Pmax1) cell_maxYP=i		
			Y_Pmax=max(Y_P(i),Y_Pmax1)
			Y_Pmin=min(Y_P(i),Y_Pmin1)
		end if
	end do


Return

End Subroutine Y_Plus