

Subroutine Tu_DPTKE

Use NEnumber		

! ##############################################################################
! IN THE SUBROUTINE, OMEGA IS CALCULATED BY USING ALL SOURCE TERMS AND EXPLICIT 
! TERMS CALCULATED IN OTHER SUBROUTINE, AND THEN VALUE OF ERROR OF OMEGA IS CALCULATED
!	
! ERROR=SIGMA (OMEGAnew-OMEGA)/SIGMA(AP*OMEGA)	
! OMEGAnew: VALUE OF CURRENT STEP	
! OMEGA : VALUE OF PREVIOUS STEP AND IN FINAL STEP, VALUE OF CURRENT ONE BY APPLYING UNDERRELAXATION FACTOR
! SIGMA : SUM OF ALL CELLS' VALUE
!
! ############################################################################## 

Implicit None
   
!Integer       :: im

Real (Kind=8) :: Fr,OmgNC,Omeganew


  ErrorO=0.0
  Fr=0.0
  do i=1,E
	Omeganew=(Omg_Exp(i)+Scomg(i)+CD(i)+Prod_Omg(i))/apom(i)
	do t=1,4
		if (Neibr(i,t)/=0) then
			OmgNC=Omega(Neibr(i,t))
		else 
			OmgNC=Omgf(i,t)
		end if
		Omeganew=Omeganew+anom(i,t)*OmgNC/apom(i)
	end do
	ErrorO=ErrorO+abs(Omeganew-Omega(i))
	Fr=Fr+abs(Omega(i))
	Omega(i)=Omeganew*AlphaO+Omega(i)*(1.0-AlphaO)
  end do
  ErrorO=ErrorO/Fr

Return

End Subroutine Tu_DPTKE
