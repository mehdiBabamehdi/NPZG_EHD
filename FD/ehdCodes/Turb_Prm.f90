
Subroutine Turb_Prm

Use NEnumber

! ##############################################################################
! IN THE SUBROUTINE, CONSTATNT OF THE SST K-OMEGA TURBULENCE MODEL ARE CALCULATED. 
! THE CONSTANTS ARE CALCULATED BASED ON THE SPECIFIC FUNCTION OF THE MODEL (F1) 
! TO CHANGE THE MODEL FROM K-OMEGA TO K-e TURBULENCE MODELS.
! ##############################################################################
Implicit None


	do i=1,E
		alp(i)=Ft1(i)*alp1+(1.0-Ft1(i))*alp2
		beta(i)=Ft1(i)*beta1+(1.0-Ft1(i))*beta2
		sgmk(i)=Ft1(i)*sgmk1+(1.0-Ft1(i))*sgmk2
		sgmo(i)=Ft1(i)*sgmo1+(1.0-Ft1(i))*sgmo2
	end do

Return

End Subroutine Turb_Prm