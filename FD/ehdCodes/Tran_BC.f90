

Subroutine Tran_BC

Use NEnumber

Implicit None


  do i = 1 , E
     do t = 1 , 4
        if (FP(i,t) == 1) then
           Intmf(i,t)  = 4.0 / 3.0 * intm(i) - 1.0 / 3.0 * intm(BNC(i,t))
           Rehmtf(i,t) = 4.0 / 3.0 * Rehmt(i) - 1.0 / 3.0 * Rehmt(BNC(i,t))
        else if (FP(i,t) == 2) then
           Intmf(i,t)  = 4.0 / 3.0 * intm(i) - 1.0 / 3.0 * intm(BNC(i,t))
           Rehmtf(i,t) = 4.0 / 3.0 * Rehmt(i) - 1.0 / 3.0 * Rehmt(BNC(i,t))
        else if (FP(i,t) == 3) then
           Intmf(i,t) = intm(i)
           Rehmtf(i,t) = Rehmt(i)
        else if (FP(i,t) == 4) then
           Intmf(i,t) = 1.0
           Rehmtf(i,t) = Remti
        else if (FP(i,t) == 6) then  ! Surface of Airfoil
           Intmf(i,t) = 4.0 / 3.0 * intm(i) - 1.0 / 3.0 * intm(BNC(i,t))
           Rehmtf(i,t) = 4.0 / 3.0 * Rehmt(i) - 1.0 / 3.0 * Rehmt(BNC(i,t))
        else if (FP(i,t) == 7) then! Symmetric Surface Upstream of the Flat Plate
           Intmf(i,t) = intm(i)
           Rehmtf(i,t) = Rehmt(i)
        end if
    end do
  end do

  Return

End Subroutine Tran_BC
