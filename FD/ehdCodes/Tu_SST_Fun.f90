

Subroutine Tu_SST_FUN

Use NEnumber

! ##############################################################################
! IN THE SUBROUTINE, BLENDING FUNCTIONS OF SST-KW TURBULENCE MODEL ARE CALCULATED.
! IN ADDITION SOME MODIFICATION REQUIRED FOR APPLYING TRANSITION MODEL SHOULD BE APPLIED.
! UPSTREAM OF PLATE IS SUPPOSED TO HAVE SMALL VALUE.
!	
! Ft1 & Ft2: BLENDING FUNCTION
! ############################################################################## 

Implicit None

Real (Kind=8) :: CDT1,CDT,CDko,F111,F112,F11,F12,F22,F21,Ry,F3,Ft1orig


	do i=1,E
		CDT1=Kx(i)*Omgx(i)+Ky(i)*Omgy(i)
		CDT=2.0*ru*sgmo2*CDT1/Omega(i)
		CDko=max(CDT,1.0e-10)
		F111=sqrt(abs(Kt(i)))/(betas*Omega(i)*wd(i))
		F112=500.0*mu/(ru*wd(i)*wd(i)*Omega(i))
		F11=max(F111,F112)
		F12=4.0*ru*sgmo2*Kt(i)/(CDko*wd(i)*wd(i))
		F22=min(F11,F12)

		Ry=ru*wd(i)*sqrt(abs(Kt(i)))/mu		! Transition Model Modification
		F3=exp(-(Ry/120.0)**8.0)

		Ft1orig=tanh(F22**4.0)
		Ft1(i)=max(Ft1orig,F3)
		F21=max(2.0*F111,F112)
		Ft2(i)=tanh(F21**2.0)

		if (Xp(i)<0.0) then		! For Upstream Of Flat Plate
			Ft1(i)=1.0e-16
			Ft2(i)=1.0e-16
		end if		
	end do
		

Return

End Subroutine Tu_SST_FUN
