
Subroutine Tr_MTRe

Use NEnumber		

Implicit None

Real (Kind=8):: RehmtNC,Rehmtnew, Fr


  ErrorR=0.0
  Fr=0.0
  do i=1,E
	Rehmtnew=(Rehmt_Exp(i)+Sc_Rehmt(i))/apmt(i)

	do t=1,4
		if (Neibr(i,t)/=0) then
			RehmtNC=Rehmt(Neibr(i,t))
		else 
			RehmtNC=Rehmtf(i,t)
		end if
		Rehmtnew=Rehmtnew+anmt(i,t)*RehmtNC/apmt(i)
	end do
	ErrorR=ErrorR+abs(Rehmtnew-Rehmt(i))
	Fr=Fr+abs(Rehmt(i))
	Rehmt(i)=Rehmtnew*AlphaR+Rehmt(i)*(1.0-AlphaR)

  end do
  ErrorR=ErrorR/Fr
  
Return

End Subroutine Tr_MTRe