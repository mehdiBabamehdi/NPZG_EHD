
subroutine MF_Final_coefficient

use NEnumber

implicit none

integer          :: i,NBT(E),T1(E),Tb(E),T3(E)
double precision :: un(N),vn(N),Pn(N),Vk(E), &
                    Cf(E),Ts(E),Cp(E),Cd(E),Cl(E), &
                    yf(E),yk(E), &
                    Mut(E), &
                    Fb(E)

real             :: mu,ru,dt,Ui,Lc,Pi,Omega,Reynolds_Number
character(10)    :: boundary(E)


common  / prop/ ru,mu,dt,Ui,P0
common  / nodv/ un,vn,pn
common  / fcoef/ Cp,Cd,Cl,Cf
common  / BounP/ NBT,T1,Tb,T3,Fb,Boundary
common  / Mut/ Mut

   !do i = mo + 1,mb
   !	Cp(i) = Pn(i) * 2.0 / ru / Ui**2.0
   !end do

   do i  =  1,NB
      if (boundary(i) == 'airfoil') then
          Yk(NBT(i)) =  (1.0 - Fb(NBT(i))) * Y(Con(NBT(i),t1(i))) + Fb(NBT(i)) * Y(Con(NBT(i),tb(i)))
          Yf(NBT(i)) = (Y(Con(NBT(i),tb(i))) + Y(Con(NBT(i),t3(i)))) / 2.0
          Vk(NBT(i)) = (1.0 - Fb(NBT(i))) * V(Con(NBT(i),t1(i)))
          Ts(NBT(i)) = (mu + Mut(i)) * Vk(NBT(i)) / (Yf(NBT(i)) - Yk(NBT(i)))
          Cf(NBT(i)) = Ts(NBT(i)) * 2.0 / ui**2.0 / ru
      end if
   end do

return

end subroutine MF_Final_coefficient
