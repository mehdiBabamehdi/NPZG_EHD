
!__________________________________________________________________________________

	 Module GCNEnumber
	 
	integer ::   N, E, &
				 n1,n2,n3,n4,n5,n6,mb,m, &
				 m10,m11,m12,m13,m14

	Integer, allocatable :: Con(:,:), Neibr(:,:), &
						    cn(:),FP(:,:)
						    

	Real (Kind=8), allocatable :: X(:),Y(:),Xp(:),Yp(:)

	Real :: L0,L1,L2,L3,L4,H,Lf

	Real,Parameter :: Lex=0.1, Len=0.1, Elc_Pos=1.0

	end module GCNEnumber
