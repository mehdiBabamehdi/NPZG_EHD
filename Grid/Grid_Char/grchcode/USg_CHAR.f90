
subroutine USg_CHAR

use GCNEnumber

implicit none

integer:: i,j,k,t,hl, &
		  CVP(20),NC(4),CV(N,20),NCV(N)

character :: form
logical:: log



	print*, '  > Part 1-1: Acheiving All Control Volumes That Each Point Belongs to'

	do j = 1,N
	   t = 0
	   do i = 1,E
		   do k = 1,4
				if(Con(i,k)==j) then
				   t = t + 1
	 			   CVP(t) = i
	 			   exit
	 		   end if
		   end do		   
		 end do
	 	 do k = 1,4
	 		 NCV(J) = t
	 		 do hl = 1,t
	 			 CV(j,hl) = CVP(hl)
	 		 end do
	    end do
write( form, '(a,i0,a)' )  '(', t+1, 'i7)'
!14  FORMAT (<t+1>I7)

		 write(100,form) t,CVP(1:t)
	 end do

	 print*, '  > Part 1-2: Acheiving All Neighbouring Control Volumes For Each One '

15  FORMAT(4I7)
	Neibr = 0
	do j = 1,E
	   t = 0
	   NC = 0
	   do i = 1,E
		  if(i/=j) then
			  log = ((Con(j,1)==Con(i,1) .or. Con(j,1)==Con(i,2) .or. &
                Con(j,1)==Con(i,3) .or. Con(j,1)==Con(i,4)) .and. &
				       (Con(j,2)==Con(i,1) .or. Con(j,2)==Con(i,2) .or. &
                Con(j,2)==Con(i,3) .or. Con(j,2)==Con(i,4)))
			  if(log) then
				  t = t + 1
				  NC(t) = i
			  end if
			  log = ((Con(j,2)==Con(i,1) .or. Con(j,2)==Con(i,2) .or. &
                Con(j,2)==Con(i,3) .or. Con(j,2)==Con(i,4)) .and. &
				       (Con(j,3)==Con(i,1) .or. Con(j,3)==Con(i,2) .or. &
                Con(j,3)==Con(i,3) .or. Con(j,3)==Con(i,4)))
			  if(log) then
   			  t = t + 1
				  NC(t) = i
			  end if
			  log = ((Con(j,3)==Con(i,1) .or. Con(j,3)==Con(i,2) .or. & 
                Con(j,3)==Con(i,3) .or. Con(j,3)==Con(i,4)) .and. &
				       (Con(j,4)==Con(i,1) .or. Con(j,4)==Con(i,2) .or. &
                Con(j,4)==Con(i,3) .or. Con(j,4)==Con(i,4)))
			  if(log) then
				  t = t + 1
				  NC(t) = i
			  end if
			  log = ((Con(j,4)==Con(i,1) .or. Con(j,4)==Con(i,2) .or. & 
                Con(j,4)==Con(i,3) .or. Con(j,4)==Con(i,4)) .and. &
				       (Con(j,1)==Con(i,1) .or. Con(j,1)==Con(i,2) .or. & 
                Con(j,1)==Con(i,3) .or. Con(j,1)==Con(i,4)))
			  if(log) then
   			  t = t + 1
				  NC(t) = i
			  end if
			  if(t==4) exit
		   end if
	   end do
   
	   do i = 1,4
		   if(NC(i)/=0) then
				log = (Con(j,1)==Con(NC(i),1) .or. Con(j,1)==Con(NC(i),2) .or. & 
               Con(j,1)==Con(NC(i),3) .or. Con(j,1)==Con(NC(i),4)) .and. &
					    (Con(j,2)==Con(NC(i),1) .or. Con(j,2)==Con(NC(i),2) .or. & 
               Con(j,2)==Con(NC(i),3) .or. Con(j,2)==Con(NC(i),4))
		   else
				log = (Con(j,1)<=mb .and. Con(j,2)<=mb)
		   end if
		   if(log) then
				 Neibr(j,1) = NC(i)
				 exit
		   end if 
	   end do	    

	   do i = 1,4
		   if(NC(i)/=0) then
				log = (Con(j,2)==Con(NC(i),1) .or. Con(j,2)==Con(NC(i),2) .or. &
               Con(j,2)==Con(NC(i),3) .or. Con(j,2)==Con(NC(i),4)) .and. &
					    (Con(j,3)==Con(NC(i),1) .or. Con(j,3)==Con(NC(i),2) .or. &
               Con(j,3)==Con(NC(i),3) .or. Con(j,3)==Con(NC(i),4))
		   else
				log = (Con(j,2)<=mb .and. Con(j,3)<=mb)
		   end if
		   if(log) then
				Neibr(j,2) = NC(i)
	 		 exit
		    end if 
	    end do	    
   
	    do i=1,4
		    if(NC(i)/=0) then
				 log = (Con(j,3)==Con(NC(i),1) .or. Con(j,3)==Con(NC(i),2) .or. &
                Con(j,3)==Con(NC(i),3) .or. Con(j,3)==Con(NC(i),4)) .and. &
					     (Con(j,4)==Con(NC(i),1) .or. Con(j,4)==Con(NC(i),2) .or. &
                Con(j,4)==Con(NC(i),3) .or. Con(j,4)==Con(NC(i),4))
		    else
				 log=(Con(j,3)<=mb .and. Con(j,4)<=mb)
		    end if
		    if(log) then
				 Neibr(j,3) = NC(i)
	 		    exit
		    end if
	    end do
		do i = 1,4
		    if(NC(i)/=0) then
				 log = (Con(j,4)==Con(NC(i),1) .or. Con(j,4)==Con(NC(i),2) .or. &
                Con(j,4)==Con(NC(i),3) .or. Con(j,4)==Con(NC(i),4)) .and. &
					     (Con(j,1)==Con(NC(i),1) .or. Con(j,1)==Con(NC(i),2) .or. & 
                Con(j,1)==Con(NC(i),3) .or. Con(j,1)==Con(NC(i),4))
		    else
				 log = (Con(j,4)<=mb .and. Con(j,1)<=mb)
		    end if
		    if(log) then
				 Neibr(j,4) = NC(i)
	 		    exit
		    end if
	    end do	    
	    write(100,15) Neibr(j,1:4)
	end do

	do i = 1,E
	 	Xp(i) = (x(con(i,1)) + x(con(i,2)) + x(con(i,3)) + x(con(i,4)))/4.0
	 	Yp(i) = (y(con(i,1)) + y(con(i,2)) + y(con(i,3)) + y(con(i,4)))/4.0
	end do


return

end subroutine USg_CHAR
