
!__________________________________________________________________________________

    subroutine Output
   
    use GGNEnumber

    implicit none

    double precision:: x(M + 1,N + 1),y(M + 1,N + 1)

    common /geom/ x,y

    open(30,file = "NZFP_GG_ECFDtest.plt")
    write(30,*)'VARIABLES = "X", "Y"'
    write(30,*)'ZONE I=',M + 1,',J=',N + 1,', C=BLUE, F=POINT'  
    write(30,*)'# Number of Boundary Divisions:',M + 1,N + 1
    write(30,*)'# Dimention of Boundary Portions:',W,Lf
	    
    21 format(2E15.7)

    do j = 1,N + 1
        do i = 1,M + 1   
 	        write(30,21)x(i,j),y(i,j)	 
        end do
    end do

	  close(30)	    
   		  
    return
    end subroutine Output
