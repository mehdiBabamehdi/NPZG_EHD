
Subroutine Tran_Coef

Use NEnumber

Implicit None

Real (Kind = 8) :: mue


   do i = 1 , E
      apin(i) = Sp_int(i)  !ru*ds(i)/dt
      apmt(i) = Sp_Rehmt(i) !ru*ds(i)/dt

      do j = 1 , 4
         if (Neibr(i,j) == 0) then
            
            if (FP(i,j) == 1) then! Solid Boundary (Mut=0)
               Mue = 0.0
            else
               Mue = Mut(i)
            end if
         else
            Mue = (1.0 - f(i,j)) * Mut(i) + f(i,j) * Mut(Neibr(i,j))
         end if
         
         anin(i,j) = max(Z,-massf(i,j)) + (mu + mue / sgmf) * L(i,j) / M(i,j)
         apin(i)   = apin(i) + anin(i,j)  !+massf(i,j)
         anmt(i,j) = max(Z,-massf(i,j)) + sgmmt * (mu + mue) * L(i,j) / M(i,j)
         apmt(i)   = apmt(i) + anmt(i,j) !+massf(i,j)
      end do
   end do

   Return 

End Subroutine Tran_Coef
