
Subroutine Normal_dis


use GCNEnumber

implicit none

integer:: i,j,t,k,nk

double precision::	Xk,Yk, &
					ndw,nd(10),sl,sa, &
					wd,d1,d2,Xm,Ym

double precision, allocatable::Xa(:),Ya(:)
					

!	open (30,file='NACA0012.dat')

	nk = n1	! for flate plate
!	nk = n4	! for airfoil

	allocate(Xa(nk + 1))
	allocate(Ya(nk + 1))

	do i = 1,nk
!		Read(30,*) Xa(i),Ya(i)	! Points on Solid Surface (Airfoil or Flat Plate)
		Xa(i) = X(i)
		ya(i) = Y(i)
	end do

!	Xa(nk + 1) = X(1)	! for airfoil
!	ya(nk + 1) = Y(1)

	Xa(nk + 1) = X(n1 + 1)
	ya(nk + 1) = Y(n1 + 1)
	
	close(30)

35  format(E24.5)

	do i = 1,E
		k = 0
		wd = 25.0		
		do j = 1,nk
			if (Xa(j) == Xa(j + 1)) then
				if ((Yp(i)<=Ya(j) .and. Yp(i)>=Ya(j + 1)) .or. (Yp(i)>=Ya(j) .and. Yp(i)<=Ya(j + 1))) then
					k = k + 1
					nd(k) = abs(Xp(i) - Xa(j))
				end if
			else if (Ya(j)==Ya(j + 1)) then
				if ((Xp(i)<=Xa(j) .and. Xp(i)>=Xa(j + 1)) .or. (Xp(i)>=Xa(j) .and. Xp(i)<=Xa(j + 1))) then
					k = k + 1
					nd(k) = abs(Yp(i) - Ya(j))
				end if
			else
				Sl = (Xa(j) - Xa(j + 1))/(Ya(j + 1) - Ya(j))
				Sa = (Ya(j + 1) - Ya(j))/(Xa(j + 1) - Xa(j))
				Xk = (Ya(j) - Yp(i) + Sl*Xp(i) - Sa*Xa(j))/(Sl - Sa)
				Yk = Ya(j) + Sa*(Xk - Xa(j))
				if ((Xk<=Xa(j) .and. Xk>=Xa(j + 1)) .or. (Xk>=Xa(j) .and. Xk<=Xa(j + 1))) then
					k = k + 1
					nd(k) = sqrt((Xp(i) - Xk)*(Xp(i) - Xk) + (Yp(i) - Yk)*(Yp(i) - Yk))
				end if					
			end if
! .......................................................................................................

			d1 = sqrt((Xp(i) - Xa(j))**2.0 + (Yp(i) - Ya(j))**2.0)
			if (d1<wd) wd = d1
			Xm = (Xa(j) + Xa(j + 1))/2.0
			Ym = (Ya(j) + Ya(j + 1))/2.0
			d2 = sqrt((Xm - Xp(i))**2.0 + (Ym - Yp(i))**2.0)
			if (d2<wd) wd = d2
			
! .......................................................................................................
		end do
		d1 = sqrt((Xp(i) - Xa(nk + 1))**2.0 + (Yp(i) - Ya(nk + 1))**2.0)
		if (d1<wd) wd = d1
		ndw = nd(1)
		if (k>1) then
			do t = 2,k
				ndw = min(ndw,nd(t))
			end do
		end if
		if (ndw<wd) wd = ndw

		if (x(i)<0.0 .and. x(i + 1)<0.0) then
			wd = 0.3
		end if

		write(100,35) wd
!		wd  : nearest distance from the wall (Turbulence model requirement)

	end do

Return

End Subroutine Normal_dis
