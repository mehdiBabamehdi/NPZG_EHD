

Subroutine Turb_Dis

Use NEnumber	

! ##############################################################################
! IN THE SUBROUTINE, DISSIPATION TERMS OF TURBULENCE MODEL (SST K-OMEGA) ARE CALCULATED 
! ##############################################################################

Implicit None
   

	do i=1,E
		Dis_k(i)=betas*ru*Omega(i)*Kt(i)*ds(i)
		Dis_omg(i)=ru*beta(i)*Omega(i)*Omega(i)*ds(i)
	end do

Return

End Subroutine Turb_Dis