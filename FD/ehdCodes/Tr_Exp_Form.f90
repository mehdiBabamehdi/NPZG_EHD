	
Subroutine Tr_Exp_Form

Use NEnumber

Implicit None


Real (Kind = 8) :: FL1,BT_exp

   do i = 1 , E
      if (CFS == 1) then   ! ... Malan et. al's Formula ..................................................

         Remc(i)    = min(0.625 * Rehmt(i) + 62.0,Rehmt(i))
         FL1        = 0.01 * exp(-0.022 * Rehmt(i) + 12.0) + 0.57
         Flength(i) = min(FL1,300.0)

      else if (CFS == 2) then  ! ... Menter's Formula ........................................................

         if(Rehmt(i) < 400.0)  &
             Flength(i) = 398.189 * 10.0**(-1.0) + (-119.270 * 10.0**(-4.0)) * Rehmt(i) &
                          + (-132.567 * 10.0**(-6.0)) * Rehmt(i)**2.0
             
             if(Rehmt(i) >= 400.0 .and. Rehmt(i) < 596.0)  &
                  Flength(i) = 263.404 + (-123.939 * 10.0**(-2.0)) * Rehmt(i) + &
                              (194.548 * 10.0**(-5.0)) * Rehmt(i)**2.0 + &
                              (-101.695 * 10.0**(-8.0)) * Rehmt(i)**3.0
             
             if(Rehmt(i) >= 596.0 .and. Rehmt(i) < 1200.0)  &
                  Flength(i) = 0.5 - (Rehmt(i) - 596.0) * 3.0 * 10.0**(-4.0)
             
             if(Rehmt(i) >= 1200.0)  Flength(i) = 0.3188

             if(Rehmt(i) <= 1870.0) &
                  Remc(i) = Rehmt(i) - (396.035 * 10.0**(-2.0) - 120.656 * 10.0**(-4) * &
                            Rehmt(i) + 868.230 * 10.0**(-6.0) * Rehmt(i)**2.0 - 696.506 * &
                            10.0**(-9.0) * Rehmt(i)**3.0 + 174.105 * 10.0**(-12) * Rehmt(i)**4.0)
             
             if(Rehmt(i) > 1870.0)  &
                  Remc(i) = Rehmt(i) - (593.11 + (Rehmt(i) - 1870.0) * 0.482)

        else if (CFS == 3) then  ! ... Sorenson's Formula .......................................................

             BT_exp     = tanh(((Rehmt(i) - 100.0) / 400.0)**4.0)
             Flength(i) = min(150.0 * exp(-(Rehmt(i) / 120.0)**1.2) + 0.1,30.0)
             Remc(i)    = BT_exp * ((Rehmt(i) + 12000.0) / 25.0) + (1.0 - BT_exp) * (7.0 * Rehmt(i) + 100.0) / 10.0

        else if (CFS == 4) then  ! ... Suluksna's Formula .......................................................

             Flength(i) = min(0.1 * exp(-0.022 * Rehmt(i) + 12.0) + 0.45,300.0)
             Remc(i) = min(max(-(0.025 * Rehmt(i))**2.0 + 1.47 * Rehmt(i) - 120.0,125.0),Rehmt(i))

        else if (CFS == 5) then  ! ... ONERA's Formula ..........................................................

            Flength(i) = exp(-1.325 * 10.0**(-8.0) * Rehmt(i)**3.0 + 7.42 * 10.0**(-6.0) * Rehmt(i)**2.0 + &
                         8.16 * 10.0**(-3.0) * Rehmt(i) + 2.5652)
            Remc(i)    = min(1.0,1.623 * 10.0**(-6.0) * Rehmt(i)**2.0 - 1.228 * 10.0**(-3.0) * Rehmt(i) + 0.849) * Rehmt(i)
        end if
    end do

    Return

End Subroutine Tr_Exp_Form
