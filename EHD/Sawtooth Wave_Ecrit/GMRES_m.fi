 Subroutine GMRES_m (rhs,an,ap,NB,E,NN,X)

  Use Geom, only: mr,itr_max
  Use ProbCons, only: tol_abs

! ****************************************************************
!  DESCRIBTION:
!
! THIS SUBROUTINE SOLVE THE A*X=Y BY USING GMRES(m) ITERATION METHOD
! IN WHICH A IS COEFFICIENT MATRIX WHICH IS NBDS-BASDED SPARSE MATRIX.
! GMRES(m) METHOD WAS USED BASED ON THE ONE INTRODUCED IN 
! YOUCEF SAAD'S BOOK.
!
!  REFERENCES:
!	Y. Saad, Iterative Methods for Sparse Linear Systems, 
!		PWS publishing company, ITP, Boston, MA, 1996.
!	Y. Saad, M. Schultz, GMRES: A generalized minimal residual 
!		algorithm for solving nonsymmetric linear systems, 
!		SIAM J. J. Statistical and Scientific Computing, 
!		7 (3)(1986),  pp. 856-869.
!  
!  PARAMETERS:
!
! E	:: 	Number of Cells
! NN	::	Number of Neighbors of each Cell
! RHS	::	RHS of the discretized equation
! ap	::	Diagonal elements of Matrix A
! an	::	Off-Diagonal elements of Matrix A
! NB	::	Matrix NB
! X		::	Inlet as Initial Condition and outlet as result of algorithm
! Itr_max :: The Maximum Number of (Outer) Iterations to Take.
! mr      :: The Maximum Number of (Inner) Iterations to Take.
! tol_abs :: Maximum Value of Error	
! *****************************************************************

 implicit none

  real ( kind = 8 ), parameter :: delta = 1.0D-03
 
  integer ( kind = 4 ) :: E,NN,NB(E,NN), &
						  i,j,k,itr,itr_used,k_copy

  real ( kind = 8 ) :: mu,rho,rho_tol,s(1:mr),v(1:E,1:mr+1),y(1:mr+1), &
					   av,c(1:mr),g(1:mr+1),h(1:mr+1,1:mr),htmp, &
					   r(1:E),ap(E),an(E,NN),rhs(1:E),x(1:E)

  itr_used = 0

  Outer: do itr = 1, itr_max

    ! ... Solve r From r=b-Ax ....

	 do i=1,E
		r(i)=rhs(i)-ap(i)*x(i)
		do j=1,NN
			if (NB(i,j)/=0) r(i)=r(i)-an(i,j)*x(NB(i,j))
		end do
	 end do

! ....................................................................................

    rho = sqrt ( dot_product ( r(1:E), r(1:E) ) )

 !   write ( *, '(A25,I8,A,E14.6)' ) '>>> ITR. of OUTER LOOP = ', itr, '  Residual = ', rho

    v(1:E,1) = r(1:E) / rho

    g(1) = rho
    g(2:mr+1) = 0.0D+00

    h(1:mr+1,1:mr) = 0.0D+00

    Inner: do k = 1, mr

      k_copy = k

      do i=1,E
			v(i,k+1)=v(i,k+1)+ap(i)*v(i,k)
			do j=1,NN
				if (NB(i,j)/=0) v(i,k+1)=v(i,k+1)+an(i,j)*v(NB(i,j),k)	
			end do						
		end do

      av = sqrt ( dot_product ( v(1:E,k+1), v(1:E,k+1) ) )

      do j = 1, k
        h(j,k) = dot_product ( v(1:E,k+1), v(1:E,j) )
        v(1:E,k+1) = v(1:E,k+1) - h(j,k) * v(1:E,j)
      end do

      h(k+1,k) = sqrt ( dot_product ( v(1:E,k+1), v(1:E,k+1) ) )

      if ( av + delta * h(k+1,k) == av ) then

        do j = 1, k
          htmp = dot_product ( v(1:E,k+1), v(1:E,j) )
          h(j,k) = h(j,k) + htmp
          v(1:E,k+1) = v(1:E,k+1) - htmp * v(1:E,j)
        end do

        h(k+1,k) = sqrt ( dot_product ( v(1:E,k+1), v(1:E,k+1) ) )

      end if

      if ( h(k+1,k) /= 0.0D+00 ) then
        v(1:E,k+1) = v(1:E,k+1) / h(k+1,k)
      end if

      if ( 1 < k ) then

        y(1:k+1) = h(1:k+1,k)

        do j = 1, k - 1
          Call mult_givens ( c(j), s(j), j, y(1:k+1) )
        end do

        h(1:k+1,k) = y(1:k+1)

      end if

      mu = sqrt ( h(k,k)**2 + h(k+1,k)**2 )
      c(k) = h(k,k) / mu
      s(k) = -h(k+1,k) / mu
      h(k,k) = c(k) * h(k,k) - s(k) * h(k+1,k)
      h(k+1,k) = 0.0D+00
      Call mult_givens ( c(k), s(k), k, g(1:k+1) )
      rho = abs ( g(k+1) )

      itr_used = itr_used + 1

      if (mod(k,100)==0)	write ( *, '(A25,I8,A,E14.6)' ) '  Iterations =   ', k, '  Residual = ', rho

      if ( rho <= tol_abs ) then
        exit
      end if

    end do Inner

    k = k_copy - 1

    y(k+1) = g(k+1) / h(k+1,k+1)

    do i = k, 1, -1
      y(i) = ( g(i) - dot_product ( h(i,i+1:k+1), y(i+1:k+1) ) ) / h(i,i)
    end do

    do i = 1, E
      x(i) = x(i) + dot_product ( v(i,1:k+1), y(1:k+1) )
    end do

    if (rho <= tol_abs ) then
      exit
    end if

  end do Outer


  Return

  End Subroutine GMRES_m