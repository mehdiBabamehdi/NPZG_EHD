
!__________________________________________________________________________________

    Function Vapp(Time1)

!    use Geom
    use ProbCons
     
    implicit none

    integer:: mf
    real:: pi,Time1,omega
	Real (Kind=8):: Vapp1,V01,Vapp  ! LMBC: Lumped Method Baundary Condition for Potential on the Surface of Virtual Electrode
	                   
	                   
    pi=4.*atan(1.)

	Vapp=0.0	!V0/2.0			! CAUTION >>> IT IS FOR SAW WAVE - IT SHOULD BE MODIFIED FOR OTHER WAVE FORMS
	mf=0

	V01=0.0

	Fourier_L: do         ! For the Fourier Transformation Terms of Square Potential Wave Shape
	
! ...... POSITIVE SAWTOOTH WAVE .............................................................................
		Omega=2.0*mf*pi/Tl             !    1	 Frequency of Wave in Fourier Series	 
		if (mf/=0)	V01=V0/pi/mf				   
		
! ----------------------------------------------------------------------------------------------------------

		Vapp1=V01*sin(omega*Time1)
		Vapp=Vapp+Vapp1
	    mf=mf+1
		if(mf==NF) exit Fourier_L
	end do Fourier_L
    
    return
    end Function Vapp
