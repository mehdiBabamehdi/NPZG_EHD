
Subroutine Tu_Omega_Explicit

Use NEnumber

! ##############################################################################
! IN THE SUBROUTINE, EXPLICIT TERM OF OMEGA EQUATION (OF DIFFISION 
! AND CONECTION TERMS)IS CALCULATED. IN CALCULATION OF CONVECTION'S, FIVE DIFFERENT 
! METHODS ARE USED. DETAILS OF METHOD CAN BE FOUND IN  VERSTEEG AND MALALASEKRA'S BOOK [1]
!	
! Omgex : EXPLICIT TERM OF CONVECTION TERM
! Omg_EX : SUM OF EXPLICIT TERMS OF CONVECTION AND DIFFUSION 
!
! --- REFERENCES: ---------------
! [1] Versteeg & Malalasekra, "An Introduction to Computational Fluid Dynamics" 2nd Ed., PEARSON PUB., 2007.
! ############################################################################## 		

Implicit None
   
Integer       :: Nt,m1,m2

Real (Kind=8) :: Omgex(E,4),Romg,Siomg,ConOmg
				 

  do i=1,E
	do t=1,4
		if(Neibr(i,t)/=0) then
			if (BNC(i,t)/=0) then
				if (AdO==1.0) 	then
					Omgex(i,t)=(-1.0*Omega(BNC(i,t))-2.0*Omega(i)+3.0*Omega(FNC(i,t)))/8.0	! QUICK
				else if (AdO==2.0) 	then
					Romg=(Omega(i)-Omega(BNC(i,t)))/(Omega(FNC(i,t))-Omega(i))
					Siomg=max(Z,min(2.0*Romg,(3.0+Romg)/4.0,2.0))				! QUICK Limiter
					Omgex(i,t)=Siomg*(Omega(FNC(i,t))-Omega(i))/2.0
				else if (AdO==3.0) 	then
					Romg=(Omega(i)-Omega(BNC(i,t)))/(Omega(FNC(i,t))-Omega(i))
					if(Romg>0.0)  Siomg=min(Romg,1.0)							! Mid-Mod Limiter
					if(Romg<=0.0) Siomg=0.0
					Omgex(i,t)=Siomg*(Omega(FNC(i,t))-Omega(i))/2.0
				else if (AdO==4.0) 	then
					Romg=(Omega(i)-Omega(BNC(i,t)))/(Omega(FNC(i,t))-Omega(i))
					Siomg=max(z,min(2.0*Romg,1.0),min(Romg,2.0))				! SUPERBEE Limiter
					Omgex(i,t)=Siomg*(Omega(FNC(i,t))-Omega(i))/2.0
				else if (AdO==5.0) 	then
					Romg=(Omega(i)-Omega(BNC(i,t)))/(Omega(FNC(i,t))-Omega(i))
					if (Romg<0.0 .or. Omega(FNC(i,t))==Omega(i)) then
						Siomg=0.0
					else
						Siomg=(Romg+abs(Romg))/(1.0+Romg)						! Van Leer Limiter
					end if
					Omgex(i,t)=Siomg*(Omega(FNC(i,t))-Omega(i))/2.0
				end if				
			else
				Omgex(i,t)=(Omega(FNC(i,t))-Omega(i))/2.0
			end if			
		end if
	end do
  end do

  CALL TurbNodal

   do i=1,E
      Omg_Exp(i)=0.0	!beta(i)*ru*Omegai**2.0 !+ru*ds(i)*Omega(i)/dt
	  do j=1,4
         select case (j)
	       case(1)
	        m1=1
		    m2=2
           case(2)
   	        m1=2
		    m2=3
           case(3)
			m1=3
		    m2=4
		   case(4)
			m1=4
		    m2=1
         end select
		if (Neibr(i,j)/=0) then
			do t=1,4
				if(i==Neibr(Neibr(i,j),t)) then
					Nt=t
					exit
				end if
			end do
			 ConOmg=min(Z,massf(i,j))*Omgex(Neibr(i,j),Nt)+max(Z,massf(i,j))*Omgex(i,j)
			 Omg_Exp(i)=Omg_Exp(i)+(mu+sgmo(i)*Mut(i))*K(i,j)/M(i,j)*(Omegan(Con(i,m2))-Omegan(Con(i,m1)))-ConOmg
		end if
	  end do
	end do

Return

End Subroutine Tu_Omega_Explicit