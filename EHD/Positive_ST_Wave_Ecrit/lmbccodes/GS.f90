

 Subroutine GS(RHS,an,ap,NB,E,NN,X)

! ***********************************************************************
!  DESCRIBTION:
!
! THIS SUBROUTINE SOLVE THE A*X=Y BY USING GAUSS-SEIDEL ITERATION METHOD
! IN WHICH A IS COEFFICIENT MATRIX WHICH IS NBDS-BASDED SPARSE MATRIX
! 
!  PARAMETERS:
!
! E		:: 	Number of Cells
! NN	::	Number of Neighbors of Each Cell
! RHS	::	RHS of the Discretized Equation
! ap	::	Diagonal Elements of Matrix A
! an	::	Off-Diagonal Elements of Matrix A
! NB	::	Matrix NB
! X		::	Inlet as Initial Condition and outlet as result of algorithm
! ***********************************************************************

 Implicit None

 Integer :: E,NN,NB(E,NN),i,jj

 Real (Kind=8) :: ap(E),an(E,4),RHS(E),X(E)

 Do i=1,E
	X(i)=(RHS(i))/ap(i)
		do jj=1,NN
			if (NB(i,jj)/=0) X(i)=X(i)+(-an(i,jj)*X(NB(i,jj)))/ap(i)
		end do
 End do


 Return

 End Subroutine GS
