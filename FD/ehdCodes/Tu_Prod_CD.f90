

Subroutine Tu_Prod_CD

Use NEnumber

! ##############################################################################
! IN THE SUBROUTINE, PRODUCTION AND DISSIPATION TERMS OF KINETIC ENERGY EQUATION 
! AND CROSS DIFFUSION TERM OF OMEGA EQUATION ARE CALCULATED.
! IN ADDITION SOME MODIFICATION REQUIRED FOR APPLYING TRANSITION MODEL SHOULD BE APPLIED.
!	
! Prod_omg : PRODUCTION TERM OF OMEGA EQUATION
! CD : CROSS DIFFUSION TERM OF OMEGA EQUATION
! Phk : PRODUCTION TERM OF KINETIC EQUATION
! ############################################################################## 	

Implicit none

Real (Kind=8) :: CDT,Pk
	do i=1,E
		Pk=Mut(i)*Str(i)*Str(i)	
		Phk(i)=min(Pk,10.0*Dis_k(i)/ds(i))
		Prod_Omg(i)=alp(i)*ru/Mut(i)*Phk(i)*ds(i)	
		CDT=Kx(i)*Omgx(i)+Ky(i)*Omgy(i)
		CD(i)=2.0*(1.0-Ft1(i))*ru*sgmo2*CDT/Omega(i)*ds(i)

		Phk(i)=Eff_int(i)*Phk(i)*ds(i)			! Transition Model Modification
		Dis_k(i)=min(max(Eff_Int(i),0.1),1.0)*Dis_k(i)		
	end do

Return

End Subroutine Tu_Prod_CD